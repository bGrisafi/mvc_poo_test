<?php

namespace Noyau\Classes;

abstract class App {
  private static $_connexion = null;

  //GETTERS
  public static function getConnexion(){
    return SELF::$_connexion;
  }

  //SETTERS
  public static function setConnexion(){
        // Paramètres de connexion
    $dsn = "mysql:host=".DBHOST.";dbname=".DBNAME;
    $param = array(\PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8');

   // Création de l'objet PDO $connexion
     try {
        SELF::$_connexion = new \PDO($dsn,DBUSER,DBPWD,$param);
     }
     catch (PDOException $e) {
          die("Problème de connexion à la base de données...");
     }
  }

  //AUTRES
  public static function start(){
    SELF::setConnexion();
  }

  public static function stop(){
    SELF::$_connexion = null;
  }
}
